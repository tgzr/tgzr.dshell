import asyncio

import async_timeout
import jsonrpc_base
import json


class RequestHandlers(object):
    def __init__(self, handlers):
        super(RequestHandlers, self).__init__()
        self._handlers = handlers

    def __contains__(self, name):
        """
        jsonrpc_base requires this but a incorrect
        answer will later raise a valid exception
        so why bother ?..
        """
        return True

    def __getitem__(self, name):
        return self._resolve_handler(name, self._handlers)

    def _resolve_handler(self, name, handlers):
        """
        Find the thing pointed by `name` in handlers.

        `name` may contain dots (`.`) to access sub-things
        in each handler.
        """
        if "." in name:
            first, rest = name.split(".", 1)
            handler = self._resolve_handler(first, handlers)
            return self._resolve_handler(rest, [handler])

        for handler in handlers:
            try:
                return getattr(handler, name)
            except AttributeError:
                continue
        raise AttributeError(f"Could not find '{name}' in request handlers: {handlers}")


class PendingMessage(object):
    """Wait for response of pending message."""

    def __init__(self):
        self._event = asyncio.Event()
        self._response = None

    async def wait(self, timeout=None):
        with async_timeout.timeout(timeout):
            await self._event.wait()
            return self._response

    @property
    def response(self):
        return self._response

    @response.setter
    def response(self, value):
        self._response = value
        self._event.set()


class JSONRPC(jsonrpc_base.Server):
    def __init__(self, ws_connection, *request_handlers, timeout=None):
        super(JSONRPC, self).__init__()
        # override the method registry for requests:
        self._server_request_handlers = RequestHandlers(request_handlers)

        self._timeout = timeout
        self._pending_messages = {}
        self._ws_connection = ws_connection

    async def send_message(self, message):
        print("sending", message)
        await self._ws_connection.send(message.serialize())
        if message.response_id:
            pending_message = PendingMessage()
            self._pending_messages[message.response_id] = pending_message
            response = await pending_message.wait(self._timeout)
            print("    --- response unlocked for", message.response_id)
            del self._pending_messages[message.response_id]
        else:
            response = None
        return message.parse_response(response)

    async def process_message(self, message):
        """
        Returns True if the message was a result pending message
        or a call request.
        """
        data = json.loads(message)
        if "method" in data:
            print("-----received request", message)
            request = jsonrpc_base.Request.parse(data)
            response = await self.async_receive_request(request)
            if response:
                await self.send_message(response)
            return True
        elif "id" in data:
            print("-----received response", message)
            self._pending_messages[data["id"]].response = data
            return True
        print("-----received unknown message")
        return False


# class Request(Message):
#     """Request a method call on the server."""

#     def __init__(self, method=None, params=None, msg_id=None):
#         self.method = method
#         self.params = params
#         self.msg_id = msg_id

#     @staticmethod
#     def parse(data):
#         """Generate a request object by parsing the json data."""
#         if 'method' not in data:
#             raise ProtocolError('Request from server does not contain method')
#         method = data.get('method')
#         params = data.get('params')
#         msg_id = data.get('id')
#         if (
#                 not isinstance(params, list)
#                 and not isinstance(params, dict)
#                 and params is not None):
#             raise ProtocolError(
#                 'Parameters must either be a positional list or named dict.')
#         return Request(method, params, msg_id)

#     @property
#     def response_id(self):
#         return self.msg_id

#     def serialize(self):
#         """Generate the raw JSON message to be sent to the server"""
#         data = {'jsonrpc': '2.0', 'method': self.method}
#         if self.params is not None:
#             data['params'] = self.params
#         if self.msg_id is not None:
#             data['id'] = self.msg_id
#         return json.dumps(data)

#     def parse_response(self, data):
#         """Parse the response from the server and return the result."""
#         if self.msg_id is None:
#             # Don't parse results for notification requests
#             return None

#         if not isinstance(data, dict):
#             raise ProtocolError('Response is not a dictionary')
#         if data.get('error') is not None:
#             code = data['error'].get('code', '')
#             message = data['error'].get('message', '')
#             raise ProtocolError(code, message, data)
#         elif 'result' not in data:
#             raise ProtocolError('Response without a result field')
#         else:
#             return data['result']

# class Caller(object):

#     def __init__(self, client, method_name, wait=True):
#         self._cleint = client
#         self._method_name = method_name
#         self._wait = wait

#     def __getattribute__(self, name):
#         if name.startswith("_"):
#             return super(Caller, self).__getattribute__(name)
#         method_name = self._method_name and self._method_name+"." or ""
#         method_name += name
#         return Caller(
#             self._client,
#             method_name,
#             wait=self._wait
#         )

#     def __call__(self, *args, **kwargs):
#         if not self._method_name:
#             raise Exception(
#                 "No method name, use dot notation to specify it."
#             )
#         req = {
#             "jsonrpc": "2.0",
#             "method": self._method_name,
#             "params": args,
#             "id": "78699d3e-02eb-46a6-a763-a1b462b54a33"
#         }
# class SimpleJSONRPC(object)

#     def __import__(self, client):
#         super(SimpleJSONRPC, self).__init__()
#         self._client = client

#     def call(self):
#         return Caller(self, wait=True)

#     def async_call(self):
#         return AsyncCall(self, wait=False)
