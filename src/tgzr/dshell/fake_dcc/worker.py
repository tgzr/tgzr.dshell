from ..qt_worker import QtWorker, qasync


class FakeDCCWorker(QtWorker):
    def __init__(self, dcc, *args, **kwargs):
        super(FakeDCCWorker, self).__init__(*args, **kwargs)

        # this will make the dcc available to the RPC:
        self.dcc = dcc

        # this simulate customization of dcc gui:
        self.dcc.add_user_button("Add Button", self._on_add_button)
        self.dcc.add_user_button("Create Items", self._on_create_items)
        self.dcc.add_user_button("Show some GUI", self._on_show_gui)

    @qasync.asyncSlot()
    async def _on_add_button(self):
        self.dcc.add_user_button("Custom Button", self._on_custom_button)

    @qasync.asyncSlot()
    async def _on_custom_button(self):
        self.dcc.create_item("Custom Button Item")

    @qasync.asyncSlot()
    async def _on_create_items(self):
        print("Qt Worker: Request Create Items")
        response = await self._post_to_dshell(
            f"FakeDCC/create_items/{self._worker_id}",
            nb=5,
        )
        print("Qt Worker: Response:", response)

    @qasync.asyncSlot()
    async def _on_show_gui(self):
        print("Qt Worker: Request show gui")

        # Note that this is completely dumb to declare a gui
        # here and show it in DShell since DShell has the pipeline
        # logic and is able to declare the right GUI, and this worker
        # should stay generic AF...
        # But I needed the disctraction, and also demo the usage of
        # GUI thru a REST endpoint O:)

        if 0:
            # Deprecated tgzr.cui lib
            from tgzr.cui.declare import declare

            with declare("hbox") as box:
                with box.form(title="FakeDCC Tool Dialog") as form:
                    form.label("Cool tool to easily create items")
                    form.field(label="Item Preffix", value_state="preffix")
                    form.field(label="Item Suffix", value_state="suffix")
                    form.field(label="How many ?", value_state="count")
                    form.button("Submit")
                    form.stretch()
                    script = """
states = context.get_states()
print(states)
                    """
                    form.handler(script=script, key="Submit")

            response = await self._post_to_dshell(
                f"FakeDCC/show_gui/{self._worker_id}", ui=box.to_json()
            )
            print("Qt Worker: Response:", response)

        else:
            namespace = "FakeDcc._on_show_gui"
            create_items_script = f"""
worker_id = get_state('{namespace}/worker_id')
endpoint = get_state('{namespace}/endpoint')

prefix = get_state('{namespace}/Prefix', 'prefix')
try:
    nb = int(get_state('{namespace}/Nb', 1))
except:
    raise Exception('Invalid value for Nb, should be an Interger !')
padding = len(get_state('{namespace}/Nb', 1))
suffix = get_state('{namespace}/Suffix', '')
use_suffix = get_state('{namespace}/use_suffix', False)
if not use_suffix:
    suffix = None

print('prefix', prefix)
print('nb', nb)
print('padding', padding)
print('suffix', suffix)
print('use_suffix', use_suffix)

# Now we should send a http request to the endpoint with those
# parameters, but this is not implemented yet so I shamelessly call the DShell
# directly:
context['dshell'].cmds.fakedcc_worker.create_items(worker_id, nb=nb, prefix=prefix, suffix=suffix, padding=padding)

            """
            # Using new lib tgzr.declare
            from tgzr.declare.default.schema import DefaultSchema as S

            def field(name, label_width=100):
                with S.HBox():
                    S.Label(name, fixed_width=label_width)
                    control = S.Input(value=f"@binded:{name}")
                return control

            with S.VBox() as UI:
                with S.Group(namespace=namespace):
                    field("Prefix")
                    field("Nb")
                    with S.HBox():
                        S.Toggle(value="@binded:use_suffix")
                        ctrl = field("Suffix", label_width=80)
                        ctrl.widget.enabled = "@binded:use_suffix"
                    S.Button("Create Items")

                    S.State(name="worker_id", value=self._worker_id)
                    S.State(
                        name="endpoint", value=f"FakeDCC/create_items/{self._worker_id}"
                    )
                    S.State(name="use_suffix", value=False)
                    S.Handle(
                        script=create_items_script, key="Create Items", action="clicked"
                    )

            ui_json = UI.json()
            response = await self._post_to_dshell(
                f"FakeDCC/show_gui/{self._worker_id}", ui=ui_json
            )
            print("Qt Worker: Response:", response)
