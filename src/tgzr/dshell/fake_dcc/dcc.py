"""
This module defines FakeDCC, a simulation of a Qt based DCC like Maya or Nuke.
"""
import random

from qtpy import QtWidgets, QtCore, QtGui


class FakeDCC(QtWidgets.QWidget):
    """
    Simulates a Qt based DCC like Maya or Nuke.

    It can run user code at startup (like maya's userSetup.py or Nuke's init.py):
        dcc = FakeDCC(startup_cb=my_callback)

    It has dcc event handlers:
        dcc.add_on_startup(my_handler)
        dcc.add_on_file_new(my_handler)
        dcc.add_on_file_opened(my_handler)
        dcc.add_on_file_closing(my_handler)
        dcc.add_on_quit(my_handler)

    It has a API for its data:
        dcc.create_item(name)
        dcc.delete_item(name)
        dcc.get_item_names()

    It has an API for its gui:
        dcc.add_user_button(button_text, callback)

    """

    def __init__(self, startup_cb):
        super(FakeDCC, self).__init__(None)

        # HANDLERS

        self._handlers = dict(
            startup=[],
            file_new=[],
            file_opened=[],
            file_closing=[],
            quit=[],
        )

        # DOCUMENT

        self._current_filename = "untitled.fake"
        self._items = []

        # GUI

        top = QtWidgets.QHBoxLayout()

        b = QtWidgets.QPushButton(self)
        b.setText("Open file")
        b.clicked.connect(self._on_open_file_button)
        top.addWidget(b)

        b = QtWidgets.QPushButton(self)
        b.setText("Close File")
        b.clicked.connect(self._on_close_file_button)
        top.addWidget(b)

        b = QtWidgets.QPushButton(self)
        b.setText("Quit")
        b.clicked.connect(self._on_quit_button)
        top.addWidget(b)

        tools = QtWidgets.QHBoxLayout()

        b = QtWidgets.QPushButton(self)
        b.setText("+")
        b.clicked.connect(self._on_create_button)
        tools.addWidget(b)

        b = QtWidgets.QPushButton(self)
        b.setText("-")
        b.clicked.connect(self._on_delete_button)
        tools.addWidget(b)

        self._current_file_label = QtWidgets.QLabel(self)
        self._list = QtWidgets.QListWidget(self)

        self._user_layout = QtWidgets.QHBoxLayout()

        vb = QtWidgets.QVBoxLayout()
        vb.addLayout(top)
        vb.addLayout(tools)
        vb.addWidget(self._current_file_label)
        vb.addWidget(self._list)
        vb.addLayout(self._user_layout)
        self.setLayout(vb)

        # FINALIZE

        self.show()
        self._update_ui()
        if startup_cb is not None:
            startup_cb(self)
        self._tigger_handlers("file_new")

    # STUFFS

    def generate_filename(self):
        sq = int(random.random() * 10)
        sh = int(random.random() * 100)
        depts = (
            "Anim",
            "Lighting",
            "Comp",
            "Grad",
        )
        dept = random.choice(depts)
        versions = (
            "",
            "-V1",
            "-V2",
            "-Blessed",
        )
        version = random.choice(versions)
        return f"sq{sq:02}_sh{sh:03}-{dept}{version}.fake"

    # HANDLERS

    def add_on_startup(self, handler):
        self._handlers["startup"].append(handler)

    def add_on_file_open(self, handler):
        self._handlers["file_open"].append(handler)

    def add_on_file_close(self, handler):
        self._handlers["file_close"].append(handler)

    def add_on_quit(self, handler):
        self._handlers["quit"].append(handler)

    def _tigger_handlers(self, event_name):
        print("FakeDCC triggering event:", event_name)
        for handler in self._handlers[event_name]:
            handler()

    # BUTTONS

    def _on_open_file_button(self):
        self._items = []
        self._current_filename = self.generate_filename()
        self._update_ui()
        self._tigger_handlers("file_opened")

    def _on_close_file_button(self):
        self._tigger_handlers("file_closing")
        self._items = []
        self._current_filename = "untitled.fake"
        self._tigger_handlers("file_new")
        self._update_ui()

    def _on_quit_button(self):
        self._tigger_handlers("file_closing")
        self.close()

    def closeEvent(self, event: QtGui.QCloseEvent) -> None:
        self._tigger_handlers("quit")
        return super().closeEvent(event)

    @QtCore.Slot()
    def _on_create_button(self):
        i = len(self._items) + 1
        self.create_item(f"Item{i}")

    @QtCore.Slot()
    def _on_delete_button(self):
        if not self._items:
            return
        name = self._items[-1]
        self.delete_item(name)

    def _update_ui(self):
        self._current_file_label.setText(
            f"<B>Current Scene:</B> {self._current_filename}"
        )
        self._list.clear()
        for name in self._items:
            QtWidgets.QListWidgetItem(name, self._list)

    def create_item(self, item_name):
        self._items.append(item_name)
        self._update_ui()
        return item_name

    def delete_item(self, item_name):
        self._items.remove(item_name)
        self._update_ui()

    def get_item_names(self):
        return [str(i) for i in self._items]

    def next_name(self):
        return f"Item_{len(self._items):03}"

    def add_user_button(self, text, callback):
        b = QtWidgets.QPushButton(self)
        b.setText(text)
        b.clicked.connect(callback)
        self._user_layout.addWidget(b)
