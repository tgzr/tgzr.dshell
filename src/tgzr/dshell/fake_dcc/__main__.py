from qtpy import QtWidgets

from .dcc import FakeDCC
from .worker import FakeDCCWorker


def run(skip_worker=False):
    """
    Run a FakeDCC with a FakeDCCWorker
    """
    app = QtWidgets.QApplication([])

    def create_worker(dcc):
        """
        This simulate the code you run at DCC startup
        like Maya's userSetup.py or Nuke's menu.py
        """
        worker = FakeDCCWorker(dcc, install_api_path="FakeDCC/install_worker")
        worker.run()

    startup_cb = create_worker
    if skip_worker:
        startup_cb = None
    dcc = FakeDCC(startup_cb=startup_cb)

    app.exec_()


if __name__ == "__main__":
    import sys

    run(skip_worker=len(sys.argv) > 1)
