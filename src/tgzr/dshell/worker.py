import os
import asyncio
import uuid
import websockets
import requests
import socket

from .simple_jsonrpc import JSONRPC


class Worker(object):
    @staticmethod
    def find_free_port(start_at=9900, max_tries=10):
        for i in range(start_at, start_at + max_tries):
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            try:
                sock.bind(("localhost", i))
            except OSError:
                print("Port is not available", i)
                continue
            else:
                print("Port is available", i)
                return i
        raise ValueError("Could not find an open port :/")

    def _get_rpc_handlers(self):
        return [self]

    def __init__(
        self,
        install_api_path="workers/install_worker_default",
        dshell_url="http://localhost:9099",
        worker_id=None,
        min_ws_control_port=9900,
    ):
        super(Worker, self).__init__()
        self._control_port = self.find_free_port(min_ws_control_port)
        self._worker_id = worker_id or str(uuid.uuid4())
        self._dshell_url = dshell_url
        self._intall_api_path = f"{install_api_path}/{self._worker_id}"
        self.dshell = None

    async def _post_to_dshell(self, api_path, **params):
        dshell_url = "http://localhost:9099"
        url = "/".join((dshell_url, api_path.lstrip("/")))
        print("Posting to DShell:", url)
        headers = {"accept": "application/json"}
        try:
            response = requests.post(url, params=params, headers=headers)
            response.raise_for_status()
        except requests.HTTPError as err:
            raise Exception(str(err))
        return response.json()

    async def declare_to_dshell(self):
        api_path = f"workers/declare/{self._worker_id}"
        print("Declaring myself to DShell", api_path)
        await self._post_to_dshell(
            api_path,
            ws_uri=f"ws://localhost:{self._control_port}",
        )

    async def request_worker_install(self):
        await self._post_to_dshell(self._intall_api_path)

    def run(self, loop=None):
        loop = loop or asyncio.get_event_loop()
        loop.run_until_complete(self._serve())

    async def _serve(self):
        print("------------> Starting Control Server")
        self._alive = asyncio.Future()
        async with websockets.serve(self._on_connect, "localhost", self._control_port):
            print("Control Server waiting for connections...")
            await self.declare_to_dshell()
            await self.request_worker_install()
            await self._alive
        print("------------> Control Server stopped.")

    async def _on_connect(self, client):
        print("--------> new connection")
        self.dshell = JSONRPC(client, *self._get_rpc_handlers())
        try:
            async for message in client:
                print("==received msg==")
                if not await self.dshell.process_message(message):
                    print("UNUSED MSG !!", message)
        except websockets.ConnectionClosed:
            print("Client connection closed.")
            return
        except Exception as err:
            print(f"!! Error on client listening: {err}")
            import traceback

            traceback.print_exc()

        print("----------> Control Connection done ?")


def run():
    worker = Worker(control_port=9900)
    worker.run()


if __name__ == "__main__":
    run()
