from __future__ import annotations

import logging
import time
import importlib
import inspect
from typing import Mapping, Optional
from fastapi import Request, status
from fastapi.responses import JSONResponse, PlainTextResponse

from fastapi import APIRouter

from ..extension.extension import hookimpl, Extension


logger = logging.getLogger(__name__)

_EXTENSION = None


def remote_py() -> RemotePy:
    global _EXTENSION
    if _EXTENSION is None:
        _EXTENSION = RemotePy()
    return _EXTENSION


class RemoteImportError(Exception):
    pass


def remote_import_error(request: Request, exc: RemoteImportError):
    return JSONResponse(
        status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, content={"message": str(exc)}
    )


router = APIRouter(prefix="/remote_py", tags=["remote_py"])


@router.get("/import/{module_name}", response_class=PlainTextResponse)
async def remote_import(module_name: str, package: Optional[str] = None):
    """Send a module source

    The `package` argument is required when performing a relative import. It
    specifies the package to use as the anchor point from which to resolve the
    relative import to an absolute import.

    """
    try:
        source = remote_py().get_module_source(module_name, package)
    except Exception as err:
        raise RemoteImportError(str(err))
    else:
        return PlainTextResponse(content=source)


class RemotePy(Extension):
    """
    Adds a **remote_py/import/{modul_name}** endpoint to the dshell.

    Once started, other process will be able to import python code from
    the dshell using `tgzr.dshell.remote_import`.

    """

    NAMESPACE_REQUIRES = ("fastapi_app",)

    def _initialize(self, *args, **kwargs):
        super(RemotePy, self)._initialize(*args, **kwargs)

    def _on_ready(self):
        # this adds our routs to the app
        # which the `fastapi_app` will serve:
        self._dshell.cmds.fastapi_app.include_router(router)

    def get_module_source(self, module_name: str, package=None):
        if package is not None and not module_name.startswith("."):
            module_name = "." + module_name
        module = importlib.import_module(module_name, package)
        source = inspect.getsource(module)
        return source


@hookimpl
def register_extension(dshell):
    return remote_py()
