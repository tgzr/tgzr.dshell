import logging
import asyncio

from ..extension.extension import hookimpl, Extension

logger = logging.getLogger(__name__)


class AsyncLoop(Extension):
    """
    This Extension provides a shared **asyncio event loop**.

    You get the loop by calling:
    ```
    dshell.cmds.async_loop.get_event_loop()
    ```

    You can register a Task to run when the DShell starts with:
    ```
    dshell.cmds.async_loop.add_run_task(my_async_task)
    ```

    See the `fastapi_example` extension for an example using `fastapi`.
    """

    def _initialize(self, *args, **kwargs):
        super(AsyncLoop, self)._initialize(*args, **kwargs)
        self._event_loop = asyncio.get_event_loop()
        self._run_tasks = []
        self._alive = None

    def get_event_loop(self):
        return self._event_loop

    def set_event_loop(self, event_loop):
        self._event_loop = event_loop

    def add_run_task(self, task):
        self._run_tasks.append(task)

    async def stop(self):
        """
        Stops the event loop, which would most probably stop the DShell.
        """
        if self._alive is None or self._alive.cancelled():
            logger.info("Cannot stop, not alive !")
            return
        self._alive.cancel()

    def _on_run(self, *args, **kwargs):
        print("async_loop:_on_run")
        loop = self._event_loop
        for task in self._run_tasks:
            print("  ", task)
            # self._event_loop.run_until_complete(task())
            loop.create_task(task())
        print(" self.alive")
        self._alive = asyncio.Future()
        self._event_loop.run_until_complete(self._alive)
        print("Done.")


@hookimpl
def register_extension(dshell):
    return AsyncLoop()
