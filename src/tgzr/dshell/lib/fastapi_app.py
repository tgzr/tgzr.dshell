from asyncio import exceptions
import logging

from fastapi import FastAPI, APIRouter
import uvicorn

from ..extension.extension import hookimpl, Extension

logger = logging.getLogger(__name__)


class FastAPIApp(Extension):
    """
    Allows extension to register routes to an internal FastAPI application.

    The resulting OpenAPI can be inspected and tested with swagger here:

    [http://localhost:9099/docs](http://localhost:9099/docs)

    """

    def _initialize(self, *args, **kwargs):
        super(FastAPIApp, self)._initialize(*args, **kwargs)

        self._app = FastAPI(
            title="TGZR - DShell API",
        )
        self._router = APIRouter()

    def _on_ready(self):
        # TODO: add a simpler way to check for extension:
        has_qt = "qtasync_loop" in self._dshell.extension_manager().get_namespaces()
        if has_qt:
            self._dshell.cmds.qtasync_loop.add_tray_action(
                "Help/API Swagger", self.open_swagger_in_browser
            )

        # Maybe get the host and port from the config here ?
        self._dshell.cmds.async_loop.add_run_task(self._start_app)

    async def _start_app(self):
        # This is derived from uvicorn.run()
        config = uvicorn.Config(
            self._app,
            host="localhost",
            port=9099,
            log_level=logging.DEBUG,
            # access_log=False,
            use_colors=False,  # :/ not on my git bash anyway...
            # Those 2 are important for to be a dshell extension:
            workers=1,
            reload=False,
        )
        server = uvicorn.Server(config=config)
        await server.serve()

    def add_exception_handler(self, exception, handler):
        print("Adding exception handler:", exception, handler)
        self._app.add_exception_handler(exceptions, handler)

    def include_router(self, router):
        print("Adding router:", router)
        self._app.include_router(router)

    def open_swagger_in_browser(self):
        import webbrowser

        webbrowser.open("http://localhost:9099/docs")


@hookimpl
def register_extension(dshell):
    return FastAPIApp()
