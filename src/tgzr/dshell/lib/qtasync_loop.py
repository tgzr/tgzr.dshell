from qtpy import QtWidgets, QtGui
import qasync

from ..extension.extension import hookimpl, Extension


class QtAsyncLoop(Extension):
    """
    Provides a QtEventLoop tuned to allow async signal/slot connections.

    Shows a Tray icon with a menu where other extensions can add actions.

    The async event loop can be used by other extensions to use Qt without
    blocking async base extensions (`fastapi_app` for example).

    """

    NAMESPACE_REQUIRES = ("async_loop",)

    def _initialize(self, dshell):
        super()._initialize(dshell)
        self._app = None
        self._tray_actions = []
        self._tray_menu = None
        self._on_app_ready_callbacks = []

    def _on_ready(self):

        app = QtWidgets.QApplication.instance()
        if app is None:
            app = QtWidgets.QApplication([])

        self._app = app

        if not QtWidgets.QSystemTrayIcon.isSystemTrayAvailable():
            raise Exception("No system tray support... goodbye then :/")

        icon = app.style().standardIcon(QtWidgets.QStyle.SP_DesktopIcon)
        if icon.isNull():
            raise Exception("No ison for tray, you wouldnt be able to quit ! :/")

        self.trayIcon = QtWidgets.QSystemTrayIcon(icon, app)
        self.trayIcon.setToolTip("TGZR Desktop Shell")

        def on_activated(reason):
            print("!!!", reason)

        self.trayIcon.activated.connect(on_activated)
        menu = self._get_tray_menu()
        self.trayIcon.setContextMenu(menu)
        self.trayIcon.show()

        self._dshell.set_notifier(self._notifier)
        # self._dshell.notify("TGZR", "Hello ! :)")

        if 0:
            # This was usefull until we had the overlays extension
            # now we'll quit using the tray icon if needed...
            self.b = QtWidgets.QPushButton()
            self.b.setText("Quit")
            self.b.clicked.connect(app.quit)
            self.b.show()

        loop = qasync.QEventLoop(app)
        self._dshell.cmds.async_loop.set_event_loop(loop)
        print("Patched Event Loop for Qt:", loop)

        print("Calling app ready callbacks:")
        for callback in self._on_app_ready_callbacks:
            print(" ", callback)
            callback()

    def _get_tray_menu(self):
        """
        This must not be called before qapp exists !
        """
        if self._tray_menu is None:
            self._tray_menu = QtWidgets.QMenu()
        self._update_tray_menu()
        return self._tray_menu

    def _update_tray_menu(self):
        if self._tray_menu is None:
            # not ready yet, it will be updated after qapp creation
            return

        submenus = {}
        self._tray_menu.clear()
        for name, callback in self._tray_actions:
            # TODO: support nested sub-menus here
            # or even better, use our declarative UI for that !!!
            names = name.rsplit("/", 1)
            try:
                submenu_name, name = names
            except ValueError:
                menu = self._tray_menu
            else:
                try:
                    menu = submenus[submenu_name]
                except:
                    sub_menu = QtWidgets.QMenu(submenu_name)
                    submenus[submenu_name] = sub_menu
                    self._tray_menu.addMenu(sub_menu)
                    menu = sub_menu
            action = menu.addAction(name)
            action.triggered.connect(callback)

        self._tray_menu.addSeparator()
        exitAction = self._tray_menu.addAction("Exit")
        exitAction.triggered.connect(self._app.quit)

    def add_tray_action(self, name, callback):
        self._tray_actions.append((name, callback))
        self._update_tray_menu()

    def _notifier(self, title, message):
        self.trayIcon.showMessage(title, message)

    def notify(self, title, message):
        self._notifier(title, message)

    def add_on_app_ready(self, callback):
        self._on_app_ready_callbacks.append(callback)
        if self._app is not None:
            callback()


@hookimpl
def register_extension(dshell):
    return QtAsyncLoop()
