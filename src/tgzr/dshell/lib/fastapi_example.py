from __future__ import annotations

import logging
from typing import Mapping, List

from fastapi import APIRouter

from ..extension.extension import hookimpl, Extension

logger = logging.getLogger(__name__)

_EXTENSION = None


def my_extension() -> FastAPIExample:
    global _EXTENSION
    if _EXTENSION is None:
        _EXTENSION = FastAPIExample()
    return _EXTENSION


router = APIRouter(prefix="/example")


@router.get("/")
async def root():
    return {"message": "hello !", "source": __file__}


@router.get("/stop_dshell", tags=["dshell"])
async def stop_dshell() -> Mapping:
    await my_extension()._dshell.cmds.async_loop.stop()
    return {"message": "hello !"}


@router.get("/db_tables", tags=["db"])
async def db_insert() -> List[str]:
    return my_extension()._dshell.cmds.db.tables()


@router.post("/db_insert/{table}", tags=["db"])
async def db_insert(table: str, doc: Mapping) -> None:
    my_extension()._dshell.cmds.db.insert(table, doc)


@router.get("/db_get/{table}", tags=["db"])
def db_get_table(self, table: str) -> Mapping:
    return [e for e in my_extension()._dshell.cmds.db.table(table)]


class FastAPIExample(Extension):
    """
    Example showing how to add http endpoints using the `fastapi_app` extension.
    """

    NAMESPACE_REQUIRES = ("fastapi_app", "db")

    def _on_ready(self):
        # this adds our routs to the app
        # which the `fastapi_app` will serve:
        self._dshell.cmds.fastapi_app.include_router(router)


@hookimpl
def register_extension(dshell):
    return my_extension()
