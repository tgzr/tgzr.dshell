from __future__ import annotations

import asyncio
import logging
from typing import Mapping, List
import json

from fastapi import APIRouter

from ..extension.extension import hookimpl, Extension


logger = logging.getLogger(__name__)

_EXTENSION = None  # singleton for the FakeDCCWorker extension


def fakedcc() -> FakeDCCWorker:
    global _EXTENSION
    if _EXTENSION is None:
        _EXTENSION = FakeDCCWorker()
    return _EXTENSION


router = APIRouter(prefix="/FakeDCC", tags=["FakeDCC"])


@router.post("/install_worker/{worker_id}")
async def default_worker_install(worker_id: str):
    """
    This is the endpoint called by default ("un-typed") worker
    to request remote installation.

    Most workers would use an endpoint in a dedicated router
    like `/Maya/install_anim_worker/...` or `/Nuke/install_worker/...`
    """
    print("!!! FakeDCC Worker Install !!!")


@router.post("/create_items/{worker_id}")
async def fakedcc_create_items(
    worker_id: str, nb: int, prefix="Remote_Created_Item_", suffix=None
):
    """
    Procedure available for FakeDCC's QtWorker.

    It will create `nb` item in the FakeDCC.
    """
    dcc = fakedcc().get_worker_rpc(worker_id)
    loop = asyncio.get_running_loop()
    print("==========>", dcc)

    async def job(dcc):
        for i in range(nb):
            item_name = f"{prefix}{i}{suffix or''}"
            await dcc.create_item(item_name)

    loop.create_task(job(dcc))


@router.post("/show_gui/{worker_id}")
async def fakedcc_show_gui(worker_id: str, ui: str):
    """
    Available for FakeDCC's QtWorker.

    It will use the `qasync_loop` to build and show a gui
    received from the DCC.
    """
    if 0:
        # Old way, using the deprecated tgzr.cui lib

        # TODO: there is probably a way to receive the ui as dict already... find it !
        ui = json.loads(ui)
        print("=====>UI")
        print(ui)
        print("=====<UI")

        if 0:
            # FIXME:
            # IDKWTF this fails but not a getattr :'(
            # but I have a demo tomorrow so quick & ugly fix is good enough !
            fakedcc()._dshell.cmds.overlays.udpate_widget_cui(
                space_name="FakeDCC Tools",
                widget_name="Create Item Tool",
                ui=ui,
            )
        udpate_widget_ui = getattr(fakedcc()._dshell.cmds.overlays, "update_widget_cui")
        udpate_widget_ui(
            space_name="FakeDCC Tools",
            widget_name="Create Item Tool",
            ui=ui,
        )
    else:
        # New way, using the tgzr.declare lib
        show_widget = getattr(fakedcc()._dshell.cmds.overlays, "show_declare_widget")
        show_widget(
            space_name="FakeDCC Tools",
            widget_name="Create Item Tool",
            UI=ui,
        )


class FakeDCCWorker(Extension):
    """
    Exposes http endpoints to control an FakeDCC connected to the DShell.

    All endpoints are listed under the `FakeDCC` group in the DShell
    swagger (Install the `overlays` extension to get a "Help" menu opening the
    swagger page for you ;) )

    Other extensions can affect the connected FakeDCC directly (without
    going thru http) using the rpc returned by:

    ```
    get_worker_rpc("some_worker_id")
    ```

    """

    NAMESPACE_REQUIRES = ("fastapi_app",)

    def __init__(self):
        super().__init__()

    def _on_ready(self):
        # this adds our routs to the app
        # which the `fastapi_app` will serve:
        self._dshell.cmds.fastapi_app.include_router(router)

    def get_worker_rpc(self, worker_id):
        """
        Use the `workers` extension to get the connection to
        this worker's rpc.
        """
        return self._dshell.cmds.workers.get_worker(worker_id).rpc.dcc

    def create_items(
        self, worker_id, nb, prefix="Item_From_DShell", suffix=None, padding=2
    ):
        dcc = self.get_worker_rpc(worker_id)
        loop = asyncio.get_running_loop()

        async def job(dcc):
            for i in range(nb):
                i = str(i).zfill(padding)
                item_name = f"{prefix}{i}{suffix or''}"
                await dcc.create_item(item_name)

        loop.create_task(job(dcc))


@hookimpl
def register_extension(dshell):
    return fakedcc()
