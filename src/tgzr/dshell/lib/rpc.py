"""

    THIS MESS IS AN SERIE OF OLD TEST, NEVERMIND !

"""
import logging
import asyncio
import websockets
from ..simple_jsonrpc import JSONRPC

from ..extension.extension import hookimpl, Extension

logger = logging.getLogger(__name__)


class RPC(Extension):
    NAMESPACE_REQUIRES = ("async_loop",)

    def _on_ready(self):
        self._alive = None
        self._dshell.cmds.async_loop.add_run_task(self._serve)

    async def _serve(self):
        print("------------> Starting Server")
        self._alive = asyncio.Future()
        async with websockets.serve(self._on_connect, "localhost", 9099):
            print("Server waiting for connections...")
            await self._alive
        print("------------> Server ended.")

    async def _on_connect(self, client):
        print("--------> new connection")
        # await client.send(
        #     json.dumps(
        #         {
        #             "jsonrpc": "2.0",
        #             "method": "identify",
        #             "params": ["TEST", "Blah !"],
        #             "id": "c19e43c0-5f87-49d9-bb2b-dc032fd7770e"
        #         }
        #     )
        # )
        # methods = dict(
        #     identify=self.identify,
        #     stop=self.stop,
        #     notify=self.notify,
        # )
        # await client.send("/identify")
        class TestHandler:
            worker = None

            async def fake_dcc_test1(self):
                await self.worker.create_item("TEST 1")
                return 1

            async def fake_dcc_test2(self, name):
                # await self.worker.create_item(name)
                # return 2
                seconds = 4
                for i in range(seconds):
                    print(f"waiting {i}/{seconds}")
                    await asyncio.sleep(1)
                return f"waited {seconds} !"

            async def wait(self, seconds=10):
                for i in range(seconds):
                    print(f"waiting {i}/{seconds}")
                    await asyncio.sleep(1)
                return f"waited {seconds} !"

        handler = TestHandler()
        worker = JSONRPC(client, handler, self._dshell)
        handler.worker = worker
        print("identity===", await worker.worker_identity())
        try:
            async for message in client:
                print("==received msg==")
                if not await worker.process_message(message):
                    print("UNUSED MSG !!", message)
                # response = await async_dispatch(
                #     message, methods=RpcMethod(self)
                # )
                # print("<==", response)
                # await client.send(response)
                # if message.startswith('/cmd'):
                #     cmd_str = message.split(' ',1)[-1]
                #     cmd_dict = json.loads(cmd_str)
                #     namespace = getattr(
                #         self._dshell.cmds,
                #         cmd_dict["namespace"]
                #     )
                #     method = getattr(
                #         namespace,
                #         cmd_dict["name"]
                #     )
                #     method(
                #         *cmd_dict["args"], **cmd_dict["kwargs"],
                #     )
                # if message == "/identify":
                #     await connection.send("/identity " + self.worker_identity())
                # else:
                #     await self._on_message(connection, message)
        except websockets.ConnectionClosed:
            print("Client connection closed.")
            return
        except Exception as err:
            print(f"!! Error on client listening: {err}")
            import traceback

            traceback.print_exc()

        print("----------> Connection done ?")

    # async def identify(self, identity, *args, **kwargs):
    #     print("+++++++IDENTITY:", identity)
    #     return Success()

    async def notify(self, title, message):
        self._dshell.cmds.qtasync_loop.notify(title, message)
        return "notified !"

    async def stop(self):
        if self._alive is None or self._alive.cancelled():
            logger.info("Cannot stop, not alive !")
            return
        self._alive.cancel()


@hookimpl
def register_extension(dshell):
    return RPC()
