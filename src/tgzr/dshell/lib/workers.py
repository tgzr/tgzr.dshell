from __future__ import annotations

import asyncio
import logging
from typing import Mapping, List

from fastapi import APIRouter
from tgzr.dshell.simple_jsonrpc import JSONRPC
import websockets

from ..extension.extension import hookimpl, Extension


logger = logging.getLogger(__name__)

_WORKERS = None  # singleton for the Worker extension


def get_workers() -> Workers:
    global _WORKERS
    if _WORKERS is None:
        _WORKERS = Workers()
    return _WORKERS


router = APIRouter(prefix="/workers", tags=["workers"])


@router.get("/list")
async def list_workers():
    return [worker.info() for worker in await get_workers().workers()]


@router.post("/declare/{worker_id}")
async def declare_worker(worker_id: str, ws_uri: str):
    """
    Workers call this to delare themselves.
    The websocket uri `ws_uri` will be used to execute
    code on them.
    """
    await get_workers().add_worker(worker_id, ws_uri)


@router.post("/install_worker_default/{worker_id}")
async def default_worker_install(worker_id: str):
    """
    This is the endpoint called by default ("un-typed") worker
    to request remote installation.

    Most workers would use an endpoint in a dedicated router
    like `/Maya/install_anim_worker/...` or `/Nuke/install_worker/...`
    """
    print("Default Worker Install:, nothing to do...")


class Worker(object):
    def __init__(self, workers, worker_id, ws_uri):
        super().__init__()
        self._workders = workers
        self._worker_id = worker_id
        self._ws_uri = ws_uri

    def info(self):
        return dict(
            worker_id=self._worker_id,
            ws_uri=self._ws_uri,
        )

    async def connect(self):
        print("Connecting to worker at", self._ws_uri)
        asyncio.create_task(self._connect())

    async def _connect(self):
        async for connection in websockets.connect(self._ws_uri):
            print("Connecting to", self._ws_uri)
            await self._on_ctrl_connection(connection)

    async def _on_ctrl_connection(self, connection):
        self._ctrl_connected = True
        self.rpc = JSONRPC(connection, self)
        try:
            async for message in connection:
                if not await self.rpc.process_message(message):
                    print("SKIPED MSG:", message)
        except websockets.ConnectionClosedError as err:
            print("!!! Connection lost:", err)
            await self._on_connection_closed()
        except Exception as err:
            print("!!! Something went horribly wrong:", err)
            print("Dropping worker...")
            await self._on_connection_closed()

    async def _on_connection_closed(self):
        await self._workders.forget_worker(self)


class Workers(Extension):
    """
    Let *workers* from other processes connect to the DShell,
    and manages the list of connected workers.

    See the **Swagger** page for the API.

    """

    NAMESPACE_REQUIRES = ("fastapi_app",)

    def __init__(self):
        super().__init__()
        self._by_worker_id = {}
        self._by_ws_uri = {}

    def _on_ready(self):
        # this adds our routs to the app
        # which the `fastapi_app` will serve:
        self._dshell.cmds.fastapi_app.include_router(router)

    async def workers(self) -> List[Worker]:
        return [w for id, w in sorted(self._by_worker_id.items())]

    async def add_worker(self, worker_id, ws_uri) -> Worker:
        worker = Worker(self, worker_id, ws_uri)
        self._by_worker_id[worker_id] = worker
        self._by_ws_uri[ws_uri] = worker
        asyncio.get_running_loop().create_task(worker.connect())
        return worker

    async def forget_worker(self, worker: Worker) -> None:
        del self._by_worker_id[worker._worker_id]
        del self._by_ws_uri[worker._ws_uri]

    def get_worker(self, worker_id):
        # print("------- get_worker", worker_id)
        # print("???", self, get_workers())
        # import pprint

        # pprint.pprint(self._by_worker_id.items())
        return self._by_worker_id[worker_id]


@hookimpl
def register_extension(dshell):
    return get_workers()
