import time
import pathlib
from typing import Mapping

from qtpy import QtWidgets, QtGui, QtCore
import qasync
import qtawesome as qta

from ..extension.extension import hookimpl, Extension

import time
import random


# DIRTY TMP HACK FOR DEMO NEEDS (faking a better system)
_DSHELL = None


class LMBMover(QtCore.QObject):

    _INSTANCE = None

    @classmethod
    def instance(cls):
        if cls._INSTANCE is None:
            cls._INSTANCE = cls()
        return cls._INSTANCE

    def __init__(self, grid_size_x=40, grid_size_y=40):
        super(LMBMover, self).__init__()
        self._down_offset = None
        self._grid_size_x = grid_size_x
        self._grid_size_y = grid_size_y

    def grid_size(self):
        """
        Returns the x and y siwe or the snaping grid.
        """
        return self._grid_size_x, self._grid_size_y

    def snap_pos_to_current_screen(self, watched, pos, snap_size):
        screen_geometry = (
            QtWidgets.QApplication.instance().desktop().screenGeometry(watched)
        )
        sl = screen_geometry.left()
        sr = screen_geometry.right()
        st = screen_geometry.top()
        sb = screen_geometry.bottom()

        size = watched.size()
        wl = pos.x()
        wr = pos.x() + size.width()
        wt = pos.y()
        wb = pos.y() + size.height()

        if abs(wl - sl) < snap_size:
            wl = sl
        elif abs(sr - wr) < snap_size:
            wl = sr - size.width()

        if abs(wt - st) < snap_size:
            wt = st
        elif abs(wb - sb) < snap_size:
            wt = sb - size.height()

        return wl, wt

    def snap_pos_to_grid(self, x, y, grid_x, grid_y):
        return int(x / grid_x) * grid_x, int(y / grid_y) * grid_y

    def modifiers_down(self):
        modifiers = QtWidgets.QApplication.queryKeyboardModifiers()
        return (
            modifiers == QtCore.Qt.ControlModifier,
            modifiers == QtCore.Qt.ShiftModifier,
            modifiers == QtCore.Qt.AltModifier,
        )

    def eventFilter(self, watched: QtCore.QObject, event: QtCore.QEvent) -> bool:
        if event.type() == event.MouseButtonPress:
            button = event.button()
            if button == QtCore.Qt.LeftButton:
                self._down_offset = event.screenPos() - watched.pos()
                return False
            elif button == QtCore.Qt.RightButton:
                self._down_offset = None
                # we want context menu on mouse down ! \o/
                watched.customContextMenuRequested.emit(event.pos())
                return True
            else:
                return super().eventFilter(watched, event)

        elif self._down_offset is not None and event.type() == event.MouseMove:
            new_pos = event.screenPos() - self._down_offset
            x = new_pos.x()
            y = new_pos.y()

            ctrl, shift, alt = self.modifiers_down()
            if ctrl:
                x, y = self.snap_pos_to_grid(
                    x,
                    y,
                    self._grid_size_x,
                    self._grid_size_y,
                )
            elif not shift:
                x, y = self.snap_pos_to_current_screen(watched, new_pos, 50)
            watched.move(x, y)
            return False
        # else:
        return super().eventFilter(watched, event)


class CollapseExpandOnOver(QtCore.QObject):
    _INSTANCE = None

    @classmethod
    def instance(cls):
        if cls._INSTANCE is None:
            cls._INSTANCE = cls()
        return cls._INSTANCE

    def eventFilter(self, watched: QtCore.QObject, event: QtCore.QEvent) -> bool:
        if event.type() == event.Enter:
            watched.expand()
        elif event.type() == event.Leave:
            watched.collapse()
        return super().eventFilter(watched, event)


class MovableWidget(QtWidgets.QWidget):
    """
    A Widget you can move with the left mous button.

    Pressing Control will snap the widget position
    to a grid.

    Pressing Shift will prevent snapping to the borders
    of the screens.

    Also, this widget will not be visible in the taskbar and
    will not have a title bar.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.installEventFilter(LMBMover.instance())

        self.setWindowFlag(QtCore.Qt.ToolTip)


class CollapsibleWidgetTitleIcon(qta.IconWidget):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._clicking = False

    def mousePressEvent(self, event):
        super().mousePressEvent(event)
        self._clicking = True

    def mouseMoveEvent(self, event):
        super().mouseMoveEvent(event)
        self._clicking = False

    def mouseReleaseEvent(self, event):
        if self._clicking:
            self.parentWidget()._on_title_icon_press(event)
        self._clicking = False


class CollapsibleWidget(MovableWidget):
    """
    A Widget that masks a part of itself when
    the mouse cursor is not over it.

    The widget can collapse on the left or on the right.
    Use `set_collapse_right(bool)` to configure it.

    Subclasses may override `_collapsed_top_left_mask`
    and `collapsed_top_right_mask` to return a custom mask (QRegion)
    to use when collapsing on the left or right side.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.installEventFilter(CollapseExpandOnOver.instance())

        self._collapse_right = False
        grid_x, grid_y = LMBMover.instance().grid_size()
        self._collapsed_width = grid_x - 2
        self._collapsed_height = grid_x - 2
        self._collapsible = True
        self._collapsed = True

        main_layout = QtWidgets.QVBoxLayout()
        main_layout.setMargin(0)
        main_layout.setContentsMargins(0, 0, 0, 0)

        self._create_title_widgets(main_layout)
        self._create_content_widgets(main_layout)
        self._create_footer_widgets(main_layout)
        self.setLayout(main_layout)

        self._update_mask()

    def set_icon(self, qtawesome_icon_name):
        self._icon.setIcon(qta.icon(qtawesome_icon_name))

    def set_title(self, title):
        self._title_label.setText(title)

    def _on_pin_tb_click(self, checked):
        if checked:
            icon = "mdi6.pin"
            self.prevent_collapse()
        else:
            icon = "mdi6.pin-outline"
            self.allow_collapse()
        self._pin_tb.setIcon(qta.icon(icon))

    def _create_title_widgets(self, main_layout):
        lo = QtWidgets.QHBoxLayout()

        self._icon = CollapsibleWidgetTitleIcon(parent=self)
        s = max(self._collapsed_width, self._collapsed_height)
        self._icon.setIconSize(QtCore.QSize(s, s))
        self._icon.setIcon(qta.icon("fa5s.compass"))
        lo.addWidget(self._icon)

        self._title_label = QtWidgets.QLabel(self)
        if self._collapsed:
            self._title_label.hide()
        lo.addWidget(self._title_label)

        lo.addStretch()

        self._pin_tb = QtWidgets.QToolButton(self)
        self._pin_tb.setCheckable(True)
        self._pin_tb.setChecked(False)
        self._pin_tb.setIcon(qta.icon("mdi6.pin-outline"))
        self._pin_tb.clicked.connect(self._on_pin_tb_click)
        lo.addWidget(self._pin_tb)

        main_layout.addLayout(lo)

    def _create_content_widgets(self, main_layout):
        pass

    def _create_footer_widgets(self, main_layout):
        pass

    def set_collapse_right(self, b):
        self._collapse_right = bool(b)

    def _collapsed_top_right_mask(self, w, h, cw, ch):
        circle = QtGui.QRegion(QtCore.QRect(w - cw, 0, cw, ch), QtGui.QRegion.Ellipse)
        rect = QtGui.QRegion(QtCore.QRect(w - cw / 2, 0, cw / 2, ch / 2))
        return circle + rect

    def _collapsed_top_left_mask(self, w, h, cw, ch):
        circle = QtGui.QRegion(QtCore.QRect(0, 0, cw, ch), QtGui.QRegion.Ellipse)
        rect = QtGui.QRegion(QtCore.QRect(0, 0, cw / 2, ch / 2))
        return circle + rect

    def _expanded_mask(self, x, y, w, h):
        """
        Return None to clear the mask and show the whole widget.
        """
        return None
        # is equivalent to this:
        return QtGui.QRegion(QtCore.QRect(0, 0, w, h))

    def _update_mask(self):
        size = self.size()
        w = size.width()
        h = size.height()
        if self._collapsed:
            cw = self._collapsed_width
            ch = self._collapsed_height
            if self._collapse_right:
                shape = self._collapsed_top_right_mask(w, h, cw, ch)
            else:
                shape = self._collapsed_top_left_mask(w, h, cw, ch)
        else:
            shape = self._expanded_mask(0, 0, w, h)
            if shape is None:
                self.clearMask()
                return
        self.setMask(shape)

    def prevent_collapse(self):
        self._collapsible = False

    def allow_collapse(self):
        self._collapsible = True

    def _on_collapsed(self):
        """Subclass can override this to react to expansion."""
        pass

    def collapse(self):
        if not self._collapsible:
            return
        if self._collapsed:
            return
        self._collapsed = True
        self._title_label.hide()
        self._pin_tb.hide()
        self._update_mask()
        self._on_collapsed()

    def _on_expanded(self):
        """Subclass can override this to react to expansion."""
        pass

    def expand(self):
        if not self._collapsed:
            return
        self._collapsed = False
        self._title_label.show()
        self._pin_tb.show()
        self._update_mask()
        self._on_expanded()

    def resizeEvent(self, event: QtGui.QResizeEvent) -> None:
        self._update_mask()
        return super().resizeEvent(event)

    def _on_title_icon_press(self, event):
        pass


class SpaceWidget(CollapsibleWidget):
    """
    A Collapsible space with dummy content widget.
    """

    def _create_content_widgets(self, main_layout):
        for i in range(random.randint(3, 10)):
            b = QtWidgets.QPushButton(f"Button {i}", self)
            main_layout.addWidget(b)


class CUISpaceWidget(CollapsibleWidget):
    """
    A Collapsible widget with a CUI body.
    """

    def _create_content_widgets(self, main_layout):
        from tgzr.cui.qt import QtRenderer

        self._host = QtWidgets.QWidget(self)
        main_layout.addWidget(self._host)
        self.renderer = QtRenderer(self._host)

    def update_ui(self, ui):
        self.renderer.render(ui)


class DeclareSpaceWidget(CollapsibleWidget):
    """
    A Collapsible widget including UI from a json
    declaration (using tgzr.declare.default.schema)
    """

    def _create_content_widgets(self, main_layout):
        from tgzr.declare.qt.renderer import QtRenderer

        self._host = QtWidgets.QWidget(self)
        main_layout.addWidget(self._host)
        self._renderer = QtRenderer(self._host)
        self._renderer.update_root_context(dshell=_DSHELL)

        S = self._renderer.schema
        with S.VBox() as includer_ui:
            S.Include(source_state="SRC_UI", trigger_state="INCLUDE_UPDATE")
        self._renderer.render(includer_ui)

    def update_ui(self, json_ui):
        UI = self._renderer.ui_from_json(json_ui)
        self._renderer.update_states({"SRC_UI": UI.dict()})
        self._renderer.update_states({"INCLUDE_UPDATE": True})


class DeclareAppSpaceWidget(CollapsibleWidget):
    """
    A Collapsible widget with a tgzr.declare.qt.app body.
    """

    def _create_content_widgets(self, main_layout):
        self._host = QtWidgets.QWidget(self)
        main_layout.addWidget(self._host)

    def embed_app(self, app):
        app.embed(self._host)


class ControlWidget(CollapsibleWidget):
    """
    A widget with controls which space to show etc...
    """

    def __init__(self, overlays_manager):
        super(ControlWidget, self).__init__(None)

        self._auto_space_toggle = True
        self._about_to_auto_space_toggle = False

        self.set_title("<H2>TGZR - Overlays</H2>")
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self._on_popup)

        self.setWindowFlag(QtCore.Qt.WindowStaysOnTopHint)

        self._overlays_manager = overlays_manager
        self.resize(10, 10)
        self.move(0, 0)
        self.show()

        self._menu = QtWidgets.QMenu(self)
        self._menu.aboutToHide.connect(self._on_popup_close)
        self._menu.addAction("Toggle Space", self._on_toggle_space)
        a = self._menu.addAction("Auto Toggle Space", self._on_toggle_auto_toggle_space)
        a.setCheckable(True)
        a.setChecked(True)
        self._menu.addSeparator()
        self._menu.addAction("Add Space", self._on_add_space)
        self._menu.addAction("Add Widget...", self._on_add_widget)
        self._menu.addSeparator()
        self._menu.addAction("Quit", self.close)

    def _create_content_widgets(self, main_layout):
        lo = QtWidgets.QHBoxLayout()

        b = QtWidgets.QPushButton(self)
        b.setText("<")
        b.clicked.connect(self._on_prev_space)
        lo.addWidget(b)

        self._current_title_label = QtWidgets.QLabel(self)
        lo.addWidget(self._current_title_label)

        b = QtWidgets.QPushButton(self)
        b.setText(">")
        b.clicked.connect(self._on_next_space)
        lo.addWidget(b)

        main_layout.addLayout(lo)

    def move(self, x, y):
        self._about_to_auto_space_toggle = False
        super().move(x, y)

    def _on_popup_close(self):
        self._collapsible = self._restore_collapsible

    def _on_popup(self, pos):
        self._about_to_auto_space_toggle = False
        self._restore_collapsible = self._collapsible
        self.prevent_collapse()
        self._menu.popup(QtGui.QCursor.pos())

    @qasync.asyncClose
    async def closeEvent(self, event: QtGui.QCloseEvent) -> None:
        await self._overlays_manager.kill_dshell()

    def _on_prev_space(self):
        self._about_to_auto_space_toggle = False
        space = self._overlays_manager.show_prev_space()
        self._current_title_label.setText(space.title)

    def _on_next_space(self):
        self._about_to_auto_space_toggle = False
        space = self._overlays_manager.show_next_space()
        self._current_title_label.setText(space.title)

    def _on_title_icon_press(self, event):
        self._on_next_space()

    def _on_add_space(self):
        i = self._overlays_manager.space_count()
        space = self._overlays_manager.create_space(f"Space #{i}")
        self._current_title_label.setText(space.title)
        space.add_widget("button", "Takavoir", 0.5, 0.5)

    def _on_add_widget(self):
        space = self._overlays_manager.get_current_space()
        space.add_widget("button", "Lalalah...", 0.5, 0.5)

    def _on_toggle_auto_toggle_space(self):
        self._auto_space_toggle = not self._auto_space_toggle

    def _on_toggle_space(self):
        space = self._overlays_manager.get_current_space()
        space.toggle()

    def _on_expanded(self):
        if self._auto_space_toggle:
            self._about_to_auto_space_toggle = True

    def _on_collapsed(self):
        if self._about_to_auto_space_toggle:
            self._on_toggle_space()
        self._about_to_auto_space_toggle = False


class Space(object):
    """
    Manages a collection of overlay widgets
    """

    def __init__(self, name, title):
        super(Space, self).__init__()
        self.name = name
        self.title = title or name
        self._hidden = True
        self._widgets = {}

    def show(self):
        print("SPACE SHOW", self.name)
        self._hidden = False
        for w in self._widgets.values():
            w.show()

    def hide(self):
        print("SPACE HIDE", self.name)
        self._hidden = True
        for w in self._widgets.values():
            w.hide()

    def toggle(self):
        if self._hidden:
            self.show()
        else:
            self.hide()

    def add_widget(self, wtype, name, x, y):
        print("Add Widget", wtype, name, x, y)
        if wtype == "CUI":
            w = CUISpaceWidget(None)
        elif wtype == "Declare":
            w = DeclareSpaceWidget(None)
        elif wtype == "DeclareApp":
            w = DeclareAppSpaceWidget(None)
        else:
            w = SpaceWidget(None)
        data = """fa-gift
            fa-git
            fa-git-square
            fa-github
            fa-github-alt
            fa-github-square
            fa-gitlab
            fa-gittip
            fa-glass
            fa-glide
            fa-glide-g
            fa-globe
            fa-google"""
        names = [i.strip()[3:] for i in data.split("\n")]
        name = random.choice(names)
        icon_name = "fa." + name
        w.set_icon(icon_name)
        w.set_title(name)
        w.expand()
        w.prevent_collapse()
        if not self._hidden:
            w.show()
        self._widgets[name] = w
        return w

    def update_widget_cui(self, widget_name, ui):
        try:
            widget = self._widgets[widget_name]
        except KeyError:
            pos = QtGui.QCursor.pos()
            widget = self.add_widget("CUI", widget_name, pos.x(), pos.y())
        widget.update_ui(ui)


class OverlaysManager(Extension):
    """
    This extension let you show "Overlay Widgets" organized into "Spaces".

    You can create widgets using json created with `tgzr.declare`:

    ```
    from tgzr.dshell.extension.extension import Extension
    from tgzr.declare.default import DefaultSchema

    class MyExtension(Extension):

        def get_my_ui(self):
            '''Returns the declaration of the GUI.'''
            with DefaultSchema.VBox() as GUI:
                DefaultSchema.Button('My Button #1')
                DefaultSchema.Button('My Button #2')
            return GUI.json()

        def _on_ready(self):
            '''Show the GUI on startup.'''
            self._dshell.cmds.overlays.show_declare_widget(
                "My Space",         # Name of the space to use/create
                "My Widget",        # Name of the widget to show/create
                self.get_my_gui(),  # UI json declaration to use
            )
    ```
    """

    NAMESPACE_REQUIRES = ("qtasync_loop",)

    def _initialize(self, *args, **kwargs):
        super(OverlaysManager, self)._initialize(*args, **kwargs)

        # DIRTY TMP HACK FOR DEMO NEEDS (faking a better system)
        global _DSHELL
        _DSHELL = self._dshell

        self._spaces = []
        self._current_space = None
        self._control_widget = None

    def _on_ready(self):
        self._dshell.cmds.qtasync_loop.add_tray_action(
            "Declare/Show Player Demo", self.show_player_demo
        )
        self._dshell.cmds.qtasync_loop.add_tray_action(
            "Declare/Show Declare Demo", self.show_declare_demo
        )
        self._dshell.cmds.qtasync_loop.add_tray_action(
            "Declare/Show Declare Editor", self.show_declare_editor
        )
        self._dshell.cmds.qtasync_loop.add_tray_action(
            "CUI/Show CUI Demo", self.show_CUI_Demo
        )
        self._dshell.cmds.qtasync_loop.add_tray_action(
            "Overlays/Toggle Controls", self.toggle_control_widget
        )
        self._dshell.cmds.qtasync_loop.add_tray_action(
            "Help/Extension View", self.show_extension_view
        )
        self._dshell.cmds.qtasync_loop.add_on_app_ready(self._on_app_ready)

    def _on_app_ready(self):
        self.create_space("_main_screen_", "Main")
        self._control_widget = ControlWidget(self)

    def show_declare_demo(self):
        from tgzr.declare.qt.demo.__main__ import app

        space_name = "Demo"
        space = None
        for this_space in self._spaces:
            if this_space.name == space_name:
                space = this_space
        if space is None:
            space_title = space_name.replace("_", " ").title()
            space = self.create_space(space_name, space_title)

        pos = QtGui.QCursor.pos()
        widget = space.add_widget("DeclareApp", "Declare Demo", pos.x(), pos.y())
        widget.embed_app(app)

    def show_player_demo(self):
        from tgzr.declare.qt.demo.player_app.__main__ import app

        space_name = "Demo"
        space = None
        for this_space in self._spaces:
            if this_space.name == space_name:
                space = this_space
        if space is None:
            space_title = space_name.replace("_", " ").title()
            space = self.create_space(space_name, space_title)

        pos = QtGui.QCursor.pos()
        widget = space.add_widget("DeclareApp", "Player Demo", pos.x(), pos.y())
        widget.embed_app(app)

    def show_declare_editor(self):
        from tgzr.declare.qt.editor.__main__ import embed

        embed()

    def show_declare_widget(self, space_name, widget_name, UI):
        space = None
        for this_space in self._spaces:
            if this_space.name == space_name:
                space = this_space
        if space is None:
            space_title = space_name.replace("_", " ").title()
            space = self.create_space(space_name, space_title)
        w = space.add_widget("Declare", widget_name, 0, 0)
        w.update_ui(UI)

    def show_extension_view(self):
        from tgzr.declare.default.schema import DefaultSchema

        show_help_script = """
dshell= context['dshell']
namespace = key
extension = getattr(dshell.cmds, namespace)
doc = extension.__class__.__doc__ or 'No doc found :/'
import textwrap
doc = textwrap.dedent(doc)
req_names = extension.NAMESPACE_REQUIRES
nb = len(req_names)
reqs = f'### Required Extensions: {nb}\\n'+', '.join(req_names)
doc = '# '+namespace+'\\n`'+repr(extension.__class__)+'`\\n'+reqs+'\\n### Doc\\n'+doc
set_state('ExtensionsView/help', doc)
        """
        S = DefaultSchema
        with S.Frame(title="DShell Extensions") as UI:
            with S.Group(namespace="ExtensionsView"):
                S.State(
                    name="help",
                    value="Click an extension to see its documentation here.",
                )
                with S.HBox():
                    with S.VBox():
                        for namespace in sorted(
                            self._dshell.extension_manager().get_namespaces()
                        ):
                            S.Button(namespace)
                        S.Stretch()
                    S.Markdown(
                        text="@binded:help",
                        min_width=400,
                        min_height=400,
                        word_wrap=True,
                    )
                S.Handle(action="clicked", script=show_help_script)
        json_ui = UI.json()
        self.show_declare_widget("DShell Admin", "Extensions", json_ui)

    def show_CUI_Demo(self):
        from tgzr.cui.qt.demo import main_window

        ui = main_window()
        self.update_widget_cui("Demo", "CUI Demo", ui)

    def toggle_control_widget(self):
        if self._control_widget is None:
            return
        self._control_widget.setVisible(not self._control_widget.isVisible())

    def space_count(self):
        return len(self._spaces)

    def create_space(self, name, title=None, make_current=True):
        space = Space(name, title)
        self._spaces.append(space)
        if make_current:
            self.set_current_space(space)
        return space

    def update_widget_cui(self, space_name, widget_name, ui):
        """
        Update the CUI in the given cui widget of the given space.
        If the given space and/or widget doesn't exist, they are
        created first.
        """
        space = None
        for this_space in self._spaces:
            if this_space.name == space_name:
                space = this_space
        if space is None:
            space_title = space_name.replace("_", " ").title()
            space = self.create_space(space_name, space_title)
        space.update_widget_cui(widget_name, ui)

    def set_current_space(self, space):
        print("==== SET CURRENT", space.name)
        for this_space in self._spaces:
            print("??", this_space.name)
            if this_space is space:
                print("!!")
                this_space.show()
            else:
                this_space.hide()
        self._current_space = space

    def get_current_space(self):
        return self._current_space

    def get_space_at(self, index):
        index = index % len(self._spaces)
        return self._spaces[index]

    def get_prev_space(self):
        if not self._spaces:
            return None
        if self._current_space is None:
            return self._spaces[0]
        current_index = self._spaces.index(self._current_space)
        return self.get_space_at(current_index - 1)

    def get_next_space(self):
        if not self._spaces:
            return None
        if self._current_space is None:
            return self._spaces[0]
        current_index = self._spaces.index(self._current_space)
        return self.get_space_at(current_index + 1)

    def show_prev_space(self):
        space = self.get_prev_space()
        self.set_current_space(space)
        return space

    def show_next_space(self):
        space = self.get_next_space()
        self.set_current_space(space)
        return space

    async def kill_dshell(self):
        await self._dshell.cmds.async_loop.stop()


@hookimpl
def register_extension(dshell):
    return OverlaysManager()
