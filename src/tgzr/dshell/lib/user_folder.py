import os
import pathlib

from tinydb import TinyDB, Query

from ..extension.extension import hookimpl, Extension


class UserFolder(Extension):
    """
    This extension managed the path to use for user files.

    All extensions should rely on this to honor command line
    options and environment variables.

    API:
    ```
    path = user_folder.get_user_folder(ensure_exists=True)

    my_extension_path = user_folder.get_extension_user_folder(
        my_extension_instance,
        ensure_exists=True,
    )
    ```

    """

    def _initialize(self, *args, **kwargs):
        super(UserFolder, self)._initialize(*args, **kwargs)
        self._user_folder = pathlib.Path.home() / "TGZR"
        self._dshell.notify("User foler", f"set to {self._user_folder}")

    def get_user_folder(self, ensure_exists=True):
        path = self._user_folder
        if ensure_exists and not path.exists():
            os.makedirs(path)
        return path

    def get_extension_user_folder(self, extension, ensure_exists=True):
        path = self._user_folder / "extensions" / extension.extension_namespace()
        if ensure_exists and not path.exists():
            os.makedirs(path)
        return path


@hookimpl
def register_extension(dshell):
    return UserFolder()
