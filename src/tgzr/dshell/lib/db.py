import time
import pathlib
from typing import Mapping

from tinydb import TinyDB, Query

from ..extension.extension import hookimpl, Extension


class DB(Extension):
    """
    The DB extension provide document storage and query for the local user.

    /!\ This database does **not** handle concurency, do not write using multiple clients !
    """

    NAMESPACE_REQUIRES = ("user_folder",)

    def _initialize(self, *args, **kwargs):
        super(DB, self)._initialize(*args, **kwargs)
        save_folder = self._dshell.cmds.user_folder.get_extension_user_folder(self)
        self._db_file = save_folder / "default.json"
        self._db = TinyDB(self._db_file)

    def _on_ready(self):
        # TMP STUFF
        log = "runs_log"
        table = self.table(log)
        table.insert({"timestamp": time.time()})

        print("Runs so far:")
        for entry in self.table(log):
            print(" ", time.ctime(entry["timestamp"]))

    def tables(self):
        return self._db.tables()

    def table(self, table: str):
        return self._db.table(table)

    def insert(self, table: str, document: Mapping):
        self._db.table(table).insert(document)


@hookimpl
def register_extension(dshell):
    return DB()
