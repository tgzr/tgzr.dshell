"""

    THIS IS JUST AN OLD TEST, NEVERMIND !

"""


import logging
import asyncio

import fastapi_jsonrpc
from pydantic import BaseModel
from fastapi import Body
import uvicorn

from ..extension.extension import hookimpl, Extension


logger = logging.getLogger(__name__)


class DShellError(fastapi_jsonrpc.BaseError):
    CODE = 5001
    MESSAGE = "RPC Error"

    class DataModel(BaseModel):
        datails: str


if 1:
    api_v1 = fastapi_jsonrpc.Entrypoint("/api/v1/jsonrpc")

    class MyError(fastapi_jsonrpc.BaseError):
        CODE = 5000
        MESSAGE = "My Error"

        class DataModel(BaseModel):
            datails: str

    @api_v1.method(errors=[MyError])
    def echo(
        data: str = Body(..., example="123"),
    ) -> str:
        if data == "error":
            raise MyError(data={"details": "error"})
        else:
            return data


class JSONRPC(Extension):
    def _on_ready(self):
        # Maybe get the host and port from the config here ?

        self._app = fastapi_jsonrpc.API(title="TGZR - DShell JSONRPC")
        if 1:
            self._app.bind_entrypoint(api_v1)
        self._dshell.cmds.async_loop.add_run_task(self._start_rpc)

    async def _start_rpc(self):
        # decorate all extension public commands:
        extension_manager = self._dshell.extension_manager()
        for namespace in extension_manager.get_namespaces():
            extension_api = fastapi_jsonrpc.Entrypoint(f"/dshell/{namespace}")
            for callable in extension_manager.get_namespace_callables(namespace):
                print(f"Decorating {namespace}.{callable.__name__}")
                extension_api.method(errors=[DShellError])(callable)
            self._app.bind_entrypoint(extension_api)

        # This is derived from uvicorn.run()
        config = uvicorn.Config(
            self._app,
            host="localhost",
            port=9010,
            log_level=logging.DEBUG,
            # access_log=False,
            use_colors=False,  # :/ not on my git bash anyway...
            # Those 2 are important for to be a dshell extension:
            workers=1,
            reload=False,
        )
        server = uvicorn.Server(config=config)
        await server.serve()


@hookimpl
def register_extension(dshell):
    return JSONRPC()
