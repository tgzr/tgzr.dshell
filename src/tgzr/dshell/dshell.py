import asyncio

import websockets

from .extension.manager import ExtensionsManager, CommandNamespaces


def default_notifier(title, message):
    print(f"[TGZR] {title} - {message}")


class DShell(object):
    def __init__(self):
        super(DShell, self).__init__()
        self._notifier = default_notifier
        self._extensions = ExtensionsManager(self)
        self.cmds = CommandNamespaces(self._extensions)
        self._extensions.register_extensions()

    def extension_manager(self):
        return self._extensions

    def run(self, *args, **kwargs):
        self._extensions.run(*args, **kwargs)

    def set_notifier(self, notifier):
        """
        Sets the callback used by `notify()`.

        The notifier must be a callable with the same
        signature as `notify()`.
        """
        self._notifier = notifier

    def notify(self, title, message):
        self._notifier(title, message)
