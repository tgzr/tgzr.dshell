"""
Import modules from the dshell `remote_py` extension

"""
import sys
import importlib
import importlib.util
from types import ModuleType
import requests


def remote_import(remote_py_url, module_name, package=None, try_local=True):
    """
    Import a module from a `remote_py` server.

    Once imported, other modules can perform standard import statement for it.
    """
    if try_local:
        try:
            module = importlib.import_module(module_name, package)
        except ImportError:
            pass
        else:
            return module

    url = f"{remote_py_url}/{module_name}"
    params = dict(package=package)
    headers = {"accept": "application/json"}
    try:
        r = requests.get(url, params=params, headers=headers)
        r.raise_for_status()
    except requests.HTTPError as err:
        raise ImportError(str(err))
    else:
        module_code = r.content  # .decode("utf-8")
    spec = importlib.util.spec_from_loader(
        module_name,
        loader=None,
        origin=url,
        is_package=False,  # ?? how sure ??
    )
    module = importlib.util.module_from_spec(spec)
    exec(module_code, module.__dict__)
    module.__file__ = url
    sys.modules[module_name] = module

    return module


def test_it():
    # somewhere:
    rpc = remote_import(
        remote_py_url="http://localhost:9099/remote_py/import",
        module_name="ws_jsonrpc",
        package="tgzr.dshell",
        try_local=False,
    )
    # this would work fine here:
    # rpc.start_server()

    # this would work fine somewhere else now:
    import ws_jsonrpc

    ws_jsonrpc.start_server(port=9098)


if __name__ == "__main__":
    test_it()
