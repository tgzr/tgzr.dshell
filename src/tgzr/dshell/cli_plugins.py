import click

from tgzr.cli import cli_plugin
from .__main__ import main


@cli_plugin
def add_cli_group(main_group):
    main_group.add_command(dshell)


@click.command()
def dshell():
    main()
