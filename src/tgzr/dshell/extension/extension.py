import pluggy

from .exceptions import RequiredExtensionError

hookimpl = pluggy.HookimplMarker("tgzr.dshell.extension")


class Extension(object):

    NAMESPACE_REQUIRES = []

    @classmethod
    def extension_namespace(cls):
        """
        Return the namespace to use for this extension.

        Beware: the extension namespace is used by other extensions
        to access this one's commands, so it is part of
        the extension API. Changing it will affect all code
        using the extension !

        Default is to use the module name (the name of
        the file containing the extension definition).
        If you define several extension in a single file,
        you will have to override this to return different
        value foe each extension.
        """
        return cls.__module__.rsplit(".", 1)[-1]

    def __init__(self):
        super(Extension, self).__init__()
        self._dshell = None

    def _is_initialized(self):
        return self._dshell is not None

    def _initialize(self, dshell):
        """
        Called by the extension manager during extension
        installation.

        All extension pointed to by self.NAMESPACE_REQUIRES
        will be initialized before this one.

        !! BEWARE !!
        When overriding this, you *MUST* call the super
        implementation ! Failing to do so will probably lead
        to an infinite loop.
        """
        self._dshell = dshell

    # ---------- Handlers

    def _on_ready(self):
        """
        Called by the extension manager once every extension
        is installed.

        This is called on all extension, with no predictable
        order.

        Subclass can override this to execute something before
        any command is fired.
        """
        pass

    def _on_run(self, *args, **kwargs):
        """
        Called by the extension manager when the application
        is ready to enter its main loop.

        The given args and kwargs come from the original call
        on `DShell.run(*args, **kwargs)`.

        Subclass can override this to execute something after
        avery extension is ready.

        This is called on all extension, with no predictable
        order. So only one of them should handle this event.
        """
        pass
