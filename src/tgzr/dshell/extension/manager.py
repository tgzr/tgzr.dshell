import inspect
import itertools
import logging

import pluggy

from .exceptions import ExtensionNamespaceError, RequiredExtensionError
from . import hookspecs

logger = logging.getLogger(__name__)


class CommandNamespaces(object):
    """
    Attribute getter for the ExtensionManager namespaces.
    """

    def __init__(self, extension_manager):
        super(CommandNamespaces, self).__init__()
        self._extension_manager = extension_manager

    def __getattribute__(self, name: str):
        if name.startswith("_"):
            return super().__getattribute__(name)
        return self._extension_manager.get_commands(name)


class ExtensionsManager(object):
    def __init__(self, dshell):
        super(ExtensionsManager, self).__init__()
        self._dshell = dshell

        self._extensions = []  # list of registered extensions
        self._extension_namespaces = {}  # namespace to Extension

        self._manager = pluggy.PluginManager("tgzr.dshell.extension")
        self._manager.add_hookspecs(hookspecs)

        from ..lib import (
            user_folder,
            db,
            async_loop,
            qtasync_loop,
            overlays,
            fastapi_app,
            fastapi_example,
            remote_py,
            workers,
            fakedcc_worker,
            # jsonrpc,
        )

        self._manager.register(user_folder)
        self._manager.register(db)
        self._manager.register(async_loop)
        self._manager.register(qtasync_loop)
        self._manager.register(overlays)
        self._manager.register(fastapi_app)
        self._manager.register(fastapi_example)
        self._manager.register(remote_py)
        self._manager.register(workers)
        self._manager.register(fakedcc_worker)
        # self._manager.register(jsonrpc)

        self._manager.load_setuptools_entrypoints("tgzr.dshell.extension")

    def _install_extension(self, extension):
        """
        Make the extension available in `dshell.cmds.<namespace>`.
        Raises an `ExtensionNamespaceError` if 2 extensions try to
        use the same namespace.
        """
        namespace = extension.extension_namespace()
        try:
            installed = self._extension_namespaces[namespace]
        except KeyError:
            self._extension_namespaces[namespace] = extension
        else:
            raise ExtensionNamespaceError(
                f"The extension {installed} is already using "
                f"the namespace '{namespace}', "
                f"cannot install {extension}."
            )

    def _initialize_extension(self, extension):
        """
        Initialize `extension` after having initialized extensions
        responsible for each namespace in this extension's
        `NAMESPACE_REQUIRES`.

        Raises a `RequiredExtensionError` if a required namespace does
        not exists (no extension declared itself for this namespace
        in `_install_extension()` )
        """
        for namespace in extension.NAMESPACE_REQUIRES:
            try:
                extension_required = getattr(self._dshell.cmds, namespace)
            except AttributeError:
                raise RequiredExtensionError(
                    f"Could not find extension namespace '{namespace}' "
                    f"(required by '{extension.extension_namespace()}')"
                )
            if not extension_required._is_initialized():
                self._initialize_extension(extension_required)
        if not extension._is_initialized():
            logger.info(f"  {extension.extension_namespace()}")
            extension._initialize(self._dshell)

    def register_extensions(self):
        logger.info("Registering Extensions...")
        self._extensions = self._manager.hook.register_extension(dshell=self)
        logger.info(f"Found {len(self._extensions)} Extensions.")

        logger.info(f"Installing extensions namespaces.")
        for extension in self._extensions:
            logger.info(f"  {extension.extension_namespace()}")
            self._install_extension(extension)

        logger.info("Initializing extensions.")
        for extension in self._extensions:
            self._initialize_extension(extension)

        logger.info("Calling on_ready on all extensions.")
        for extension in self._extensions:
            logger.info(f"  {extension.extension_namespace()}")
            extension._on_ready()

    def get_namespaces(self):
        return self._extension_namespaces.keys()

    def get_namespace_callables(self, namespace):
        extension = self._extension_namespaces[namespace]
        callables = []
        for name in dir(extension):
            if name.startswith("_"):
                continue
            o = getattr(extension, name)
            if inspect.ismethod(o) and inspect.isfunction(
                getattr(extension.__class__, name)
            ):
                # does not match classmethods (trust me ;))
                callables.append(o)
        return callables

    def get_commands(self, namespace):
        try:
            return self._extension_namespaces[namespace]
        except KeyError:
            raise ValueError(
                f"No extension registered for namespace '{namespace}',"
                f"(we have: {sorted(self._extension_namespaces.keys())})",
            )

    def run(self, *args, **kwargs):
        """
        Calls run(*args, **kwargs) on all extensions.

        Note that extensions are not ordered and so the
        calls to run() are not orderer either.
        """
        for extension in self._extensions:
            extension._on_run(*args, **kwargs)
