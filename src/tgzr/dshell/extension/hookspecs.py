import pluggy

hookspec = pluggy.HookspecMarker("tgzr.dshell.extension")


@hookspec
def register_extension(dshell):
    """
    Return an instance of dshell.extensions.extension.Extension()
    to register in this dshell
    """
