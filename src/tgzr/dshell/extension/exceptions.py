class RequiredExtensionError(RuntimeError):
    pass


class ExtensionNamespaceError(ValueError):
    pass
