import logging

from .dshell import DShell

def main():
    logging.basicConfig(
        format="[%(levelname)s %(name)s] %(message)s",
        # level=logging.DEBUG,
    )

    d = DShell()
    d.run()

if __name__ == "__main__":
    main()