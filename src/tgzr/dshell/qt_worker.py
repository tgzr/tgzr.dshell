import asyncio

from qtpy import QtWidgets
import qasync

from .worker import Worker


class QtWorker(Worker):
    """
    A Worker compatible with Qt event loop.
    """

    def run(self, loop: qasync.QEventLoop = None):
        if loop is None:
            app = QtWidgets.QApplication.instance()
            loop = qasync.QEventLoop(app)
            asyncio.set_event_loop(loop)
        super(QtWorker, self).run(loop)
