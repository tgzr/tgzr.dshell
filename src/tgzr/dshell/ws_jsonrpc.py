"""

    Trying to get a super simplified websocker JSONRPC server/client.
    Spoil alert: it does not work at all (yet) !

"""
import inspect
import asyncio
from jsonrpc_base import jsonrpc
import websockets
import uuid
import json


class NoHandlerError(ValueError):
    pass


class _RequestBuilder(object):
    """
    synopsis:
        rpc_connection.call.something(20)
        rpc_connection.notify.spam.foo(bar="baz")

    """

    def __init__(self, rpc, notify_only, *method_names):
        super(_RequestBuilder, self).__init__()
        self._rpc = rpc
        self._notify_only = notify_only
        self._method_names = method_names

    def __getattr__(self, method_name):
        if method_name.startswith("_"):  # prevent calls for private methods
            raise AttributeError("invalid attribute '%s'" % method_name)
        return _RequestBuilder(
            self._rpc, self._notify_only, *self._method_names + (method_name,)
        )

    async def __call__(self, *args, **kwargs):
        if args and kwargs:
            raise ValueError(
                f"JSON-RPC can only send args OR kwargs, got {args} and {kwargs}"
            )
        if self._notify_only:
            await self._rpc._send_notify_request(
                ".".join(self._method_names), *args, *kwargs
            )
            return None
        else:
            return await self._rpc._send_call_request(
                ".".join(self._method_names), *args, *kwargs
            )


class RPCConnection(object):
    def __init__(self, ws_client, *handlers):
        super(RPCConnection, self).__init__()
        self._ws_client = ws_client
        self._call_handlers = handlers

        self._pending_result_events = {}
        self._timeout_requests = []

        self.call = _RequestBuilder(self, notify_only=False)
        self.notify = _RequestBuilder(self, notify_only=True)

    def _resolve_handler(self, handler, method_names):
        if not method_names:
            return handler
        this_method_name = method_names[0]
        next_method_names = method_names[1:]
        try:
            next_handler = getattr(handler, this_method_name)
        except AttributeError:
            raise NoHandlerError()
        return self._resolve_handler(next_handler, next_method_names)

    def _get_handler(self, method_path):
        for handler in self._call_handlers:
            try:
                return self._resolve_handler(handler, method_path.split("."))
            except NoHandlerError:
                continue
        raise NoHandlerError(method_path)

    async def _send_notify_request(self, method, *args, **kwargs):
        """
        Call the remote method with args or kwargs
        and DONT WAIT for a return value.
        """
        await self._send_request(None, method, args, kwargs)

    async def _send_call_request(self, method, *args, **kwargs):
        """
        Call the remote method with args or kwargs
        and WAIT for its return value.
        """
        msg_id = str(uuid.uuid4())
        await self._send_request(msg_id, method, args, kwargs)

    async def _send_request(self, msg_id, method, args, kwargs):
        timeout = 2  # seconds or None to wait forever

        if args and kwargs:
            raise ValueError(
                f"JSON-RPC can only send args OR kwargs, got {args} and {kwargs}"
            )
        params = args or kwargs or None
        data = dict(
            jsonrpc="2.0",
            method=method,
        )
        if params is not None:
            data["params"] = params
        if msg_id is not None:
            data["id"] = msg_id

        request = json.dumps(data)
        await self._ws_client.send(request)

        if msg_id is not None:
            event = asyncio.Event()
            self._pending_result_events[msg_id] = event
            print("Added pending event", msg_id, len(self._pending_result_events))
            try:
                response = await asyncio.wait_for(event.wait(), timeout)
                print("    --- response unlocked for", msg_id)
                return response
            except asyncio.TimeoutError:
                self._timeout_requests.append(request)
                print("! REQUEST TIMEOUT !:", request)
                return None
            finally:
                print("Droped pending event", msg_id, len(self._pending_result_events))
                del self._pending_result_events[msg_id]

    async def process_message(self, message):
        """
        Returns True if the message was handled.
        """
        data = json.loads(message)
        if not isinstance(data, dict):
            return False

        try:
            jsonrpc_version = data["jsonrpc"]
        except Exception:
            # Not a JSONRPC request
            return False
        if jsonrpc_version != "2.0":
            # Not a compatible version of JSONRPC
            return False

        if "result" in data or "error" in data:
            print("<result", data)
            msg_id = data["id"]
            event = self._pending_result_events[msg_id]
            print(":result for", msg_id)
            event.set()
            return True
        else:
            print("<request", data)
            msg_id = data.get("id")  # id None is for notifications
            method_name = data["method"]
            params = data["params"]
            args = []
            kwargs = {}
            if isinstance(params, dict):
                kwargs = params
            else:
                args = params
            print("  args, kwargs:", args, kwargs)

            result = None
            error = None
            try:
                handler = self._get_handler(method_name)
            except NoHandlerError as err:
                print("NoHanderError!", err)
                error = dict(error=-32601, message="Method not found")
            else:
                print("  handler:", handler)
                try:
                    if inspect.iscoroutine(handler):
                        result = await handler(*args, **kwargs)
                    else:
                        result = handler(*args, **kwargs)
                except Exception as err:
                    print("!! Server Error !!", err)
                    error = dict(error=-32000, message=f"Server error: {err}")
            if msg_id is not None:
                # return result only for non-notification calls
                await self._send_result(msg_id, result, error)
            return True

    def _process_request(self, data):
        print("<new request", data)
        return "OKay!"

    async def _send_result(self, msg_id, result=None, error=None):
        data = dict(
            jsonrpc="2.0",
            id=msg_id,
        )
        if result is not None:
            data["result"] = result
        if error is not None:
            data["error"] = error
        print(">result", data)
        await self._ws_client.send(json.dumps(data))


#
#
#


def start_server(*args, **kwargs):
    return example_server(*args, **kwargs)


def example_server(host="localhost", port=9099):
    class ServerCmds:
        def do_some_server_thing(self, nb):
            print("Did server thing", nb)

        def get_some_server_data(self, nb):
            return f"ServerData for {nb}"

    async def on_connection(ws_connection):
        print("New Connection !")
        server_cmds = ServerCmds()
        client = RPCConnection(ws_connection, server_cmds)

        async for message in ws_connection:
            print("SERVER GOT MESSAGE")
            await client.process_message(message)

        print("Connection closed.")

    async def main():
        print("Server starts.")
        async with websockets.serve(on_connection, host, port):
            print("Waiting for new connection...")
            await asyncio.Future()

    asyncio.run(main())


def example_client():
    uri = "ws://localhost:9099/api/v1/jsonrpc"

    class ClientCmds:
        def do_some_client_thing(self, nb):
            print("Did client thing", nb)
            return nb - 1

    async def client():
        async with websockets.connect(uri) as ws_connection:
            client_cmds = ClientCmds()
            server = RPCConnection(ws_connection, client_cmds)

            return_value = await server.notify.do_some_server_thing(4)
            print("Got:", return_value)
            return_value = await server.call.get_some_server_data(3)
            print("Got:", return_value)
            return_value = await server.notify.do_some_server_thing(2)
            print("Got:", return_value)

            async for message in ws_connection:
                print("CLIENT GOT MESSAGE")
                await server.process_message(message)

    asyncio.run(client())


if __name__ == "__main__":
    import sys

    if sys.argv[-1] == "s":
        example_server()
    else:
        example_client()
